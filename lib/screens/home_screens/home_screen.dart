import 'package:firebase_test_example/services/authentication/auth_services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final authServiceProvider = Provider.of<AuthServices>(context);
    return Scaffold(
      appBar: AppBar(title: const Text("Home Screen"), actions: [
        IconButton(
          icon: const Icon(Icons.exit_to_app),
          onPressed: () async=>await authServiceProvider.logout(),
        ),
      ]),
    );
  }
}
